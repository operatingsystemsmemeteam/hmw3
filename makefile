
# makefile for threaded_sort.c
#
# Tyler Manifold & Isaac Murillo
# CSCI 40300

CXX = gcc
STD = c11

sort: threaded_sort.c
	$(CXX) -std=$(STD) -lpthread $^ -o $@ 

sort2: threaded_sort2.c
	$(CXX) -std=$(STD) -lpthread $^ -o $@ -g

