
// threaded_sort.c
//
// Tyler Manifold & Isaac Murillo
// CSCI 40300
//
// This program uses 3 threads to sort an array of size-n integers.
// "A list of integers is divided into two smaller lists of equal size. Two separate
//  threads (which we will term sorting threads) sort each sublist using a sorting
//  algorithm of your choice. The two sublists are then merged by a third thread --
//  a merging thread -- which merges the two sublists into a single sorted list."
// -- Project 2, p. 199

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>


typedef struct sort_data
{
	int* data;
	size_t n;
} sd;

struct sort_data* data;
struct sort_data* LHS;
struct sort_data* RHS;

pthread_t thread_left, thread_right, thread_merge;

int* unsorted;
int* sorted;

// write the given array to the console
void print(int* arr, int size)
{
	//printf("\n");
	//printf("tid %u \n %s: ", pthread_self(), __func__);
	//printf("\n");
	for (int i = 0; i < size; i++)
	{
		printf("%5d ", arr[i]);

		if ((i+1) % 15 == 0)
			printf("\n");
	}

//	printf("\n");
}

// merge LHS and RHS into sorted
void* merge(int* size)
{	
	int j = 0;
	int k = 0;

	pthread_join(thread_left, NULL);
	pthread_join(thread_right, NULL);

	printf("tid %u: merging %d integers.\n\n", pthread_self(), *size);
/*
	printf("LHS: ");
	print(LHS->data, LHS->n);
	
	printf("RHS: ");
	print(RHS->data, RHS->n);
*/
	for (int i = 0; i < *size; i++)
	{
		//printf ("%u,  LHS %u, RHS %u\n", i, LHS->data[j], RHS->data[k]);

		if (j < LHS->n && k < RHS->n)
		{
			if (LHS->data[j] < RHS->data[k])
			{
				sorted[i] = LHS->data[j];
				j++;
			}
			else
			{
				sorted[i] = RHS->data[k];
				k++;
			}
		}
		else if (j == LHS->n) // LHS iterator has reached the end of the array
		{
			// fill remainder of sorted with RHS
			while (i < *size)
			{
				sorted[i] = RHS->data[k];
				i++;
				k++;
			}
		}
		else if (k == RHS->n) // RHS iterator has reached the end of the array
		{
			// fill reaminder of sorted with LHS

			while (i < *size)
			{
				sorted[i] = LHS->data[j];
				i++;
				j++;
			}

		}
		//printf("\tsorted[%u]: %d\n\n", i, sorted[i]);
	}

	print(sorted, *size);

	pthread_exit(NULL);
}

// bubblesort the array given in data->unsorted  and store in data->sorted
// return the pointer to the newly sorted array segment
void* sort(void* data)
{
	struct sort_data* d = (struct sort_data*) data;

	printf("tid %u: sorting data\n", pthread_self());
	
	//d->data = (int*) malloc(d->n * sizeof(int));

	// copy the unsorted array into sorted memory space
	//for (int k = 0; k < d->n; k++)
//	{
//		d->sorted[k] = d->unsorted[k];
//	}

//	printf("array to be sorted: ");
//	print(d->sorted, d->n);

	// sort d->unsorted into d->sorted
	
	for (int i = 0; i < d->n; i++)
	{
		for (int j = 0; j < d->n; j++)
		{
			if (d->data[i] < d->data[j])
			{
			//	printf ("swap: %d <-> %d\n", d->sorted[i], d->sorted[i+1]);
				int temp = d->data[i];

				d->data[i] = d->data[j];

				d->data[j] = temp;
			}

		}
	}
	
	//print(d->data, d->n);
	
	pthread_exit(NULL);
}

// get a sublist of the given array, from indeces start to end.
int* sublist(int* data, int start, int end)
{
	//printf ("tid %u: Creating sublist from [%d,%d]: ", pthread_self(), start, end - 1);

	int* temp = calloc(end - start, sizeof *temp);

	int j = 0;

	for (int i = start; i < end; i++)
	{
		temp[j] = data[i];
		//printf ("temp[j],data[i] = (%d, %d)\n", temp[j],data[i]);
		j++;
	}

	//printf("\n");

	//print(temp, end - start);
	return temp;
}

// run a trial with n integers
void run(int n)
{
	data =  malloc(sizeof *data);

	unsorted = calloc(n, sizeof *unsorted);
	sorted   = calloc(n, sizeof *sorted);
	
	srand(time(NULL));

	//printf("Allocated %ub for unsorted array. ", sizeof(int) * n);
	//printf("Allocated %ub for sorted array.\n", sizeof(int) * n);

	printf("\nFilling unsorted array %d with random integers:\n", n);

	for (int i = 0; i < n; i++)
	{
		unsorted[i] = (rand() % n) + 1;
		//printf("%d\t%d\n", i, unsorted[i]);
	}

	print(unsorted, n);
	printf("\n\n");
	
	LHS = malloc(sizeof *LHS);

	LHS->n = n/2;
	LHS->data = sublist(unsorted, 0, n / 2);

	RHS = malloc(sizeof *LHS);

	RHS->n = n/2;
	RHS->data = sublist(unsorted, n/2, n);

	pthread_create(&thread_left, NULL, (void*) sort, LHS);
	pthread_create(&thread_right, NULL, (void*) sort, RHS);
	pthread_create(&thread_merge, NULL, (void*) merge, &n);

	pthread_join(thread_merge, NULL);

/*	
	printf("LHS: ");
	print(LHS->data, n/2);

	printf("\nRHS: ");
	print(RHS->data, n/2);
*/
	printf("\nDeallocating resources.\n");
	free(unsorted);
	free(sorted);
	free(data);
	
	free(LHS->data);
	free(LHS);
	
	free(RHS->data);
	free(RHS);

	printf("Done.\n\n");
}

int main()
{
	clock_t t;
	double duration;

	// 100 integers

	t = clock();	
	
	run(100);

	t = clock() - t;

	duration = ((double) t) / CLOCKS_PER_SEC * 1000;

	printf("Sorted %d integers in %f ms.\n", 100, duration);

	// 1,000 integers
	t = clock();	
	
	run(1000);

	t = clock() - t;

	duration = ((double) t) / CLOCKS_PER_SEC * 1000;

	printf("Sorted %d integers in %f ms.\n", 1000, duration);

	// 10,000 integers
	t = clock();	
	
	run(10000);

	t = clock() - t;

	duration = ((double) t) / CLOCKS_PER_SEC * 1000;

	printf("Sorted %d integers in %f ms.\n", 10000, duration);

	return 0;
}
