
// threaded_sort.c
//
// Tyler Manifold & Isaac Murillo
// CSCI 40300
//
// This program is a modified version of threaded_sort.c
// Sort an array of size n integers across m threads where m = 2, 4, or 8

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>

pthread_t* workers;

int* nums; // the list of integers


typedef struct sort_data
{
	int* data;
	int start; // starting index of array "slice"
	int next; // starting index of the second subarray to be merged
	int size;
} sd;


// write the given array to the console
void print(int* arr, int size)
{
	//printf("\n");
	//printf("tid %u \n %s: ", pthread_self(), __func__);
	//printf("\n");
	for (int i = 0; i < size; i++)
	{
		printf("%5d ", arr[i]);

		if ((i+1) % 16 == 0)
			printf("\n");
	}

	printf("\n");
}
// same as print but specify # of items per line
void printn(int* arr, int size, int n)
{
	//printf("\n");
	//printf("tid %u \n %s: ", pthread_self(), __func__);
	//printf("\n");
	for (int i = 0; i < size; i++)
	{
		printf("%5d ", arr[i]);

		if ((i+1) % n == 0)
			printf("\n");
	}

	printf("\n");
}

int calc_power(int m)
{
	int x = 0; // exponent for m = 2^x

	while (m != 1)
	{
		m /= 2;
		x++;
	}

	return x;
}

// get a sublist of the given array, from indeces start to end.
struct sort_data* sublist(int* data, int start, int end)
{
	printf ("tid %u %s: Creating sublist from [%d,%d] \n", pthread_self(), __func__,  start, end - 1);
	
	struct sort_data* t = malloc(sizeof *t);
	
	t->data = malloc((end - start) * sizeof *t->data);
	t->start = start;
	t->size = end - start;
	
	//int* t = (int*) malloc((end - start) * sizeof(int));

	int j = 0;

	for (int i = start; i < end; i++)
	{
		t->data[j] = data[i];
		//printf ("t[j],data[i] = (%d, %d)\n", t[j],data[i]);
		j++;
	}

	//printf("\n");

	//print(t, end - start);
	return t;
}

// sort the subarray given by input, and write the result back to unsorted.
void* sort(void* input)
{
	struct sort_data* d = (struct sort_data*) input;

	printf("tid %u %s: sorting subarray\n", pthread_self(), __func__);

	for (int i = 0; i < d->size; i++)
	{
		for (int j = 0; j < d->size; j++)
		{
			if (d->data[i] < d->data[j])
			{
				int temp = d->data[i];

				d->data[i] = d->data[j];

				d->data[j] = temp;
			}
		}
	}

	for (int x = 0; x < d->size; x++)
	{
		nums[d->start + x] = d->data[x];
	}
	
	//print(d->data, d->size);
}

// called by a thread to merge the subarrays given by m. The starting positon and size of one half of the subarray is given.
//  The starting postion of the second half is calculated by m->start + m->size
//  The end of the second half is calculated by m->start + m->size * 2
void* thread_merge(void* m)
{
	struct sort_data* merge_info = (struct sort_data*) m;

	int j = merge_info->start;
	int k = merge_info->next;

	int end = merge_info->next + merge_info->size;
	
	int* merge_temp = calloc(merge_info->size * 2, sizeof *merge_temp);

	
	int thread_num = merge_info->start / merge_info->size;

	printf("\ntid %u %s: merging from [%u, %u]\n", pthread_self(), __func__, merge_info->start, end - 1);
	//printf("tid %u %s: \t j=%d, k=%d, end=%d\n", pthread_self(), __func__, j, k, end);

	for (int i = 0; i < end; i++)
	{
		if (j < merge_info->next && k < end)
		{
			if (nums[j] < nums[k])
			{
				merge_temp[i] = nums[j];
				j++;
			}	
			else if (nums[j] >= nums[k]) // nums[j] < nums[k]
			{
				merge_temp[i] = nums[k];
				k++;
			}
		}
		else if (j == merge_info->next) // j has reached end of LHS
		{
			//printf("tid %u %s: iterator j has reached end of subarray. j=%d\n", pthread_self(),__func__, j);
			j++;
			while (k < end)
			{
				// fill remainder of merge_temp with whats left of right side
				merge_temp[i] = nums[k];
				//printf("tid %u %s: merge_temp[%d] = nums[%d] = %d\n",pthread_self(),__func__, i, k, nums[k]);
				i++;
				k++;
			}
		}
		else if (k == end) // k has reached end of RHS
		{
			//printf ("tid %u %s: iterator k has reached end of subarray. k=%d\n", pthread_self(),__func__, k);
			k++;
			while (j < merge_info->next)
			{
				// fill remainder of merge_temp with whats left of left side
				merge_temp[i] = nums[j];
				//printf("tid %u %s: merge_temp[%d] = nums[%d] = %d\n", pthread_self(),__func__, i, j,  nums[j]);
				i++;
				j++;
			}
		}
		//printf("tid %u %s: merge_temp[%d] = %d\n", pthread_self(), __func__, i, merge_temp[i]);
	}

	// write merge_temp back into nums

	printf("tid %u %s: writing merge_temp back into nums for range [%d,%d]\n", pthread_self(), __func__, merge_info->start, end - 1);

	int v = 0;

	for (int q = merge_info->start; q < end; q++)
	{
		nums[q] = merge_temp[v];
		v++;

		//printf("tid %u %s: nums[%d] = merge_temp[%d] = %d\n", pthread_self(), __func__, q, q, nums[q]);
	}
	
	printf("\ntid %u %s: wrote merge_temp back into nums\n", pthread_self(), __func__);

	free(merge_temp);
	free(merge_info);

	pthread_exit(NULL);
}

// merge sorted subarrays in unsorted via m threads and store in sorted
void merge(int n, int m)
{	
	int size = n / m;

	int s = 0;

	for (int i = 0; i < m; i++)
	{
			if (i == 0 || i % 2 == 0)
			{
				struct sort_data* merge_info = malloc(sizeof *merge_info);
				
				merge_info->size = size;
				merge_info->start = i * size;
				merge_info->next  = (i+1) * size;
				//printf("%s: created merge_info:\n\tsize: %d\n\tstart: %d\n\tnext: %d\n", __func__, merge_info->size, merge_info->start, merge_info->next);

				printf("tid %u %s: initiating merge on thread%d. Subarray size %d. Start index %d.\n", pthread_self(), __func__, i, merge_info->size, merge_info->start);

				// workers[i] will merge unsorted[size * i] with unsorted[size * i+1]

				s = pthread_create(&workers[i], NULL, (void*) thread_merge, merge_info);

				if (s != 0)
					printf("could not start thread%d\n", i);
			}
	}

	for (int j = 0; j < m; j++)
	{	
		if (j == 0 || j % 2 == 0)
			pthread_join(workers[j], NULL);
	}
}

void run (int n, int m)
{

	printf ("\n----------------------------------------\nSorting %d integers using %d threads\n----------------------------------------\n\n", n, m);

	nums = calloc(n, sizeof *nums);
//	merge_temp = calloc(n, sizeof *merge_temp);

	printf("tid %u %s: allocated data for merge_temp and nums.\n", pthread_self(), __func__);

	int subarr_length = n / m;

	srand(time(NULL));

	printf("\ntid %u %s: Filling nums array with random %d integers:\n", pthread_self(), __func__, n);

	for (int i = 0; i < n; i++)
	{
		nums[i] = (rand() % n) + 1;
		//printf("%d\t%d\n", i, nums[i]);
	}

	print(nums, n);
	
	printf("tid %u %s: spawning %d threads.\n", pthread_self(), __func__, m);

	workers = malloc(m * sizeof *workers);
	
	int start = 0;
	int end = subarr_length;

	int s;

	for (int z = 0; z < m; z++)
	{
		s = pthread_create(&(workers[z]), NULL, (void*) sort, sublist(nums,start, end));
		
		start+= subarr_length;
		end += subarr_length;
		
		if (s == 0)
			printf("tid %u %s: created thread%d: tid %u\n", pthread_self(), __func__, z, workers[z]);
		else
			printf("tid %u %s: could not create thread%d!\n", pthread_self(), __func__, z);
	}
	
	for (int j = 0; j < m; j++)
	{
		pthread_join(workers[j], NULL);
	}

	int iter = calc_power(m) ;

	int merge_threads = m;

	for (int h = 0; h < iter; h++)
	{
		printf ("merge threads: %d\n", merge_threads);
		merge(n, merge_threads);
		merge_threads = merge_threads / 2;
	}
	
	print(nums, n);

	//printf("%d = 2^%d\n", m, calc_power(m));
	
	
	free(workers);
	free(nums);


	//pthread_exit(NULL);
}


int main()
{

	clock_t t;
	double duration;

	t = clock();

	run(128, 2);

	t = clock() - t;
	duration = ((double) t) / CLOCKS_PER_SEC * 1000;

	printf("Sorted %d integers with %d threads in %f ms.\n\n", 128, 2, duration);

	t = clock();

	run(128, 4);

	t = clock() - t;
	duration = ((double) t) / CLOCKS_PER_SEC * 1000;

	printf("Sorted %d integers with %d threads in %f ms.\n\n", 128, 4, duration);

	t = clock();

	run(128, 8);

	t = clock() - t;
	duration = ((double) t) / CLOCKS_PER_SEC * 1000;

	printf("Sorted %d integers with %d threads in %f ms.\n\n", 128, 8, duration);

	t = clock();

	run(1024, 2);

	t = clock() - t;
	duration = ((double) t) / CLOCKS_PER_SEC * 1000;

	printf("Sorted %d integers with %d threads in %f ms.\n\n", 1024, 2, duration);

	t = clock();

	run(1024, 4);

	t = clock() - t;
	duration = ((double) t) / CLOCKS_PER_SEC * 1000;

	printf("Sorted %d integers with %d threads in %f ms.\n\n", 1024, 4, duration);

	t = clock();

	run(1024, 8);

	t = clock() - t;
	duration = ((double) t) / CLOCKS_PER_SEC * 1000;

	printf("Sorted %d integers with %d threads in %f ms.\n\n", 1024, 8, duration);

	return 0;
}
